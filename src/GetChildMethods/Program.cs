﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GetChildMethods
{
    //EXAMPLE 5 : Demonstrates several of the GetChild* methods.
    //EXAMPLE 6 : Demonstrates some ways to iterate over direct child nodes by indexs
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();
            Chilkat.Xml child = null;

            bool success;
            success = xml.LoadXmlFile("get_child.xml");
            if (success != true) {
                Console.WriteLine(xml.LastErrorText);
                return;
            }

            //  The NumChildren property contains the number of direct
            //  child nodes.  Note: The child nodes under "xyz" are NOT
            //  direct children of "root".  Therefore, the "root" node has
            //  7 direct children
            Console.WriteLine("NumChildren : " + Convert.ToString(xml.NumChildren));

            //  Iterate over the direct children by index. The first child
            //  is at index 0.
            int i;
            for (i = 0; i<=xml.NumChildren - 1; i++) {
                //   access the tag and content directly by index:
                Console.WriteLine(Convert.ToString(i) + " : " + xml.GetChildTagByIndex(i) + " : " + xml.GetChildContentByIndex(i));
            }

            Console.WriteLine("--------");

            //  Do the same as the above loop, but get the child node
            //  and access the Tag and Content properties:
            for (i = 0; i <= xml.NumChildren - 1; i++) {
                child = xml.GetChild(i);
                Console.WriteLine(Convert.ToString(i) + " : " + child.Tag + " : " + child.Content);
            }

            Console.WriteLine("--------");

            //  Do the same as the above loop, but instead of creating
            //  a new object instance for each child, call GetChild2 to
            //  update the object's reference instead.
            for (i = 0; i < xml.NumChildren; i++) {
                //  Navigate to the Nth child.
                success = xml.GetChild2(i);
                Console.WriteLine(Convert.ToString(i) + " : " + xml.Tag + " : " + xml.Content);
                //  Navigate back up to the parent:
                success = xml.GetParent2();
            }

            Console.WriteLine("--------");

            // Examine the result:
            Console.WriteLine(xml.GetXml());

        }
    }
}
