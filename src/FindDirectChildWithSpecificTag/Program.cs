﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindDirectChildWithSpecificTag
{
    //EXAMPLE 3 : Demonstrates how to find a direct child having a specific tag.
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();
            Chilkat.Xml child = null;

            bool success;

            success = xml.LoadXmlFile("fruit.xml");
            if (success != true){
                Console.WriteLine(xml.LastErrorText);
                return;
            }

            //  Find the direct child node having the tag "meat", and
            //  return a new instance of the XML object referencing the
            //  child node, if found.
            child = xml.FindChild("meat");

            if (child == null){
                Console.WriteLine("No direct child having the tag \"meat\" was found.");
            }
            else{
                Console.WriteLine("Content = " + child.Content);
            }

            //  The same can be accomplished without creating a new
            //  XML object instance.  Instead, the FindChild2 method updates
            //  the caller's internal reference to the found child
            success = xml.FindChild2("meat");
            if (success == true) {
                // Success!The xml object now references the found child.
                Console.WriteLine("Content = " + xml.Content);
                //  Restore the reference back to the parent.
                success = xml.GetParent2();
            }
            else {
                Console.WriteLine("No direct child having the tag \"meat\" was found.");
            }

        }
    }
}
