﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MethodsForGettingAttributes
{
    //EXAMPLE 4 : Demonstrates some methods for getting attribute name/values.
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();
            Chilkat.Xml carNode = null;
            int numAttr;
            int horsepower;
            int i;

            bool success;
            success = xml.LoadXmlFile("car.xml");
            if (success != true) {
                Console.WriteLine(xml.LastErrorText);
                return;
            }

            //  Navigate to the "car" node, which is the 1st child:
            carNode = xml.FirstChild();

            //  Get the value of the "model" attribute:
            Console.WriteLine("model = " + carNode.GetAttrValue("model"));

            //  Get the value of the "hp" attribute as an integer:
            horsepower = carNode.GetAttrValueInt("hp");
            Console.WriteLine("horsepower = " + Convert.ToString(horsepower));

            //  Iterate over the attributes and show the name/value of each:
            numAttr = carNode.NumAttributes;

            i = 0;
            while (i < numAttr)
            {
                Console.WriteLine(carNode.GetAttributeName(i) + " : " + carNode.GetAttributeValue(i));
                i = i + 1;
            }

            }
        }
    }
