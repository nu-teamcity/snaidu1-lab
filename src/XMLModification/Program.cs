﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XMLModification
{

    class XMLExample
    {
        public static void readXML(string filename)
        {
            // 1. Load XML File
            var xmlDocument = XDocument.Load(@filename);
                        
            // 2. Find the Element
            var majorNum = xmlDocument.Element("ApexClass").Element("packageVersions");

            //3. Change the value
            majorNum.SetElementValue("majorNumber", "3");
            majorNum.SetElementValue("minorNumber", "4");

            // 4. Create a FileStream object
            FileStream fs = new FileStream("XMLFile1.xml", FileMode.Open);

            // 5. Save the changes in the XML file
            xmlDocument.Save(fs);

        }

    }

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("XML Examples !!!");

            XMLExample.readXML(@"XMLFile1.xml");

        }
    }
}
