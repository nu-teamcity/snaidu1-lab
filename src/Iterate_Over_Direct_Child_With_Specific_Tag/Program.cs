﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iterate_Over_Direct_Child_With_Specific_Tag
{
    //EXAMPLE 7 : Demonstrates how to iterate over direct children having a specific tag.
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();
            Chilkat.Xml child = null;
            int i;
            int numWithTag;
            bool success;

            success = xml.LoadXmlFile("fruit.xml");
            if (success != true) {
                Console.WriteLine(xml.LastErrorText);
                return;
            }

            //   Get the number of direct children having the tag "fruit";
            numWithTag = xml.NumChildrenHavingTag("fruit");

            if (numWithTag > 0)
            {
                for (i = 0; i <= numWithTag - 1 ; i++)
                {
                    child = xml.GetNthChildWithTag("fruit", i);
                    Console.WriteLine(Convert.ToString(i) + " : " + child.Tag + " : " + child.Content);
                }


                Console.WriteLine("-------------");

                //  Do the same as the above loop, but instead of creating
                //  a new object instance for each child, call GetNthChildWithTag2 to
                //  update the object's reference instead.
                for (i = 0; i <= numWithTag - 1 ; i++)
                {
                    //  Navigate to the Nth child.
                    success = xml.GetNthChildWithTag2("fruit", i);
                    Console.WriteLine(Convert.ToString(i) + " : " + xml.Tag + " : " + xml.Content);
                    //  Navigate back up to the parent:
                    success = xml.GetParent2();
                }

                Console.WriteLine("-------------");

            }

        }
    }
}
