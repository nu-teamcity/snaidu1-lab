﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreateNewChildNode
{
    //EXAMPLE 8 : Demonstrates how to create a new child in an XML document.
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();

            xml.Tag = "abc";

            //  Create a new child  with no content
            //  and return the newly created child node:
            Chilkat.Xml childnode = null;
            childnode = xml.NewChild("xyz", "");

            //  Create a child under childNode, but instead call
            //  NewChild2 which doesn't return the newly created child node:
            childnode.NewChild2("mmm", "123");

            Console.WriteLine(xml.GetXml());
        }
    }
}
