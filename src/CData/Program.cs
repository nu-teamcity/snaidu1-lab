﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CData
{
    //EXAMPLE 1 : Demonstrates how to force the content of a node to be encapsulated in CDATA
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();

            Chilkat.Xml child1 = null;
            Chilkat.Xml child2 = null;
            Chilkat.Xml child3 = null;

            xml.Tag = "root";

            child1 = xml.NewChild("year", "2009");

            child2 = xml.NewChild("junk1", "abc .. < & > 123");

            child3 = xml.NewChild("junk2", "abc .. < & > 123");
            child3.Cdata = true;

            Console.WriteLine(xml.GetXml());
            

        }
    }
}
