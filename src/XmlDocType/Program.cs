﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XmlDocType
{
    //EXAMPLE 2 : Demonstrates setting an XML document's DOCTYPE.
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml root = new Chilkat.Xml();

            root.Tag = "html";

            root.DocType = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">";

            root.NewChild2("body", "This is the HTML body");

            Console.WriteLine(root.GetXml());

        }
    }
}
