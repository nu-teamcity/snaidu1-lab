﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RemoveChildNodes
{
    //EXAMPLE 8 : Demonstrates various methods for removing child nodes from an XML document.
    public class Program
    {
        public static void Main(string[] args)
        {
            Chilkat.Xml xml = new Chilkat.Xml();
            Chilkat.Xml xyz = null;

            bool success;

            success = xml.LoadXmlFile("fruit.xml");
            if (success != true) {
                Console.WriteLine(xml.LastErrorText);
                return;
            }

            xml.RemoveChild("fruit");


            Console.WriteLine("Result with all direct children having a tag equal to \"fruit\" removed:");
            Console.WriteLine(xml.GetXml());

            //--------------------------------------------------------------------------
            // Restore the original XML;
            success = xml.LoadXmlFile("fruit.xml");

            //  The RemoveChildWithContent method removes the child
            //  having the exact content specified, regardless of the tag.
            xml.RemoveChildWithContent("pear");

            //  Show the resulting XML:
            Console.WriteLine("Result with the node containing \"pear\" removed:");
            Console.WriteLine(xml.GetXml());

            //--------------------------------------------------------------------------
            // Restore the original XML;
            success = xml.LoadXmlFile("fruit.xml");

            //  The RemoveChildByIndex method removes the Nth direct
            //  child.  Indexing begins at 0.  The "xyz" child is at index 4:
            xml.RemoveChildByIndex(4);

            //  Show the resulting XML:
            //  Notice that the entire "xyz" subtree is removed.
            Console.WriteLine("Result with the node at index 4 removed:");
            Console.WriteLine(xml.GetXml());

            // --------------------------------------------------------------------------
            // Restore the original XML;
            success = xml.LoadXmlFile("fruit.xml");

            //  Navigate to the node with tag "xyz";
            xyz = xml.FindChild("xyz");

            //  Remove the "xyz" subtree making it it's own XML document
            //  with the "xyz" node at the root:
            xyz.RemoveFromTree();

            //  Show both XML documents:
            Console.WriteLine(xyz.GetXml());
            Console.WriteLine(xml.GetXml());

            //  Also, the TreeId property is an integer value assigned
            //  to nodes in an XML document.  All nodes belonging to
            //  the same XML document will have the same TreeId.
            //  Notice that the "xyz" node now has a different TreeId:
            Console.WriteLine("xyz TreeId :" + Convert.ToString(xyz.TreeId));
            Console.WriteLine("xml TreeId :" + Convert.ToString(xml.TreeId));

        }
    }
}
